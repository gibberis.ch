import grok
import user
import zope.interface

from layer import BullschitRESTLayer
    
template = """
<html>
<head>
</head>
<body>
  <h1>%s</h1>

  <ul>
    %s
  </ul>

</body>
</html>
"""


class Bullschit(grok.Application, grok.Container):
    """
    the top level container contains per-user bullschit generators.
    """

class BullschitREST(grok.REST):
    """
    RESTful interface to Bullschit
    """
    grok.context(Bullschit)

    def GET(self):
        """ PLEASE REFACTOR ME """
        values = ['<li><a href="%s">%s</a></li>' % (grok.url(
                    self.request, self.context, user), user)
                  for user in sorted(self.context.keys())]
        return template % ('Users', '\n'.join(values))

    def POST(self):
        self.context[self.body] = user.UserFolder(self.body)
        url = grok.url(self.request, self.context, self.body)
        self.response.setHeader('Location', url)
        self.response.setStatus(201)
        return url

class BullschitRESTProtocol(grok.RESTProtocol):
    grok.name('bullschit')
    grok.layer(BullschitRESTLayer)

class Index(grok.View):
    pass # see app_templates/index.pt
