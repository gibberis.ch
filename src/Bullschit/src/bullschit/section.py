import grok
from persistent.list import PersistentList


class BullschitSection(grok.Model):

    def __init__(self):
        self.schnippets = PersistentList()
    

class SectionREST(grok.REST):
    grok.context(BullschitSection)

    def GET(self):
        from app import template
        return template % ('Schnippets', '\n'.join('<li>%s</li>' % s for s in self.context.schnippets))
    
    def DELETE(self):
        del self.context.__parent__[self.context.name]

    def POST(self):
        self.context.schnippets.extend([line.strip() for line in self.body.splitlines()])
