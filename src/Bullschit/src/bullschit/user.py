import grok
import zope.interface
import section
import random

from gibberis.ch import chomsky
from layer import BullschitRESTLayer

class UserFolder(grok.Container):
    def __init__(self, name):
        self.name = name
        grok.Container.__init__(self)

class Gibberisch(grok.View):
    grok.layer(BullschitRESTLayer)
    grok.context(UserFolder)

    def render(self):
        size = int(self.request.get('size', '10').strip())
        sections = self.request.get('sections', '').split(',')
        return chomsky.chomsky(size, *[self.context[s].schnippets for s in sections])

class Slideshow(grok.View):
    grok.context(UserFolder)
    
    def foo(self):
        size = random.randint(2,4)
        sections = self.request.get('sections', 'leadins,subjects,verbs,objects')
        sections = sections.split(',')
        sections = [self.context[s].schnippets for s in sections]
        return [chomsky.chomsky(1, *sections) for i in xrange(size)]

class UserFolderREST(grok.REST):
    grok.context(UserFolder)

    def GET(self):
        """ PLEASE REFACTOR ME """
        from app import template
        values = ['<li><a href="%s">%s</a></li>' % (grok.url(
                    self.request, self.context, user), user)
                  for user in sorted(self.context.keys())]
        return template % ('Sections', '\n'.join(values))


    def DELETE(self):
        del self.context.__parent__[self.context.name]

    def PUT(self):
        for section_name in self.body.splitlines():
            section_name = section_name.strip()
            self.context[section_name] = section.BullschitSection()
        return "Everything is fine"

    def POST(self):
        self.context[self.body] = section.BullschitSection()
        url = grok.url(self.request, self.context, self.body)
        self.response.setHeader('Location', url)
        self.response.setStatus(201)
        return url
